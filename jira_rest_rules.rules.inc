<?php

/**
 * @file
 * Jira REST API Rules integration.
 */

/**
 * Implements hook_rules_action_info().
 *
 * A very basic Create Jira issue action.
 */
function jira_rest_rules_rules_action_info() {
  $items['jira_rest_rules_create_issue'] = array(
    'label' => t('Create Jira Issue'),
    'group' => t('Jira REST Rules'),
    'named parameter' => TRUE,
    'parameter' => array(
      'project' => array(
        'type' => 'text',
        'label' => t('Project key'),
        'restriction' => 'input',
      ),
      'issuetype' => array(
        'type' => 'text',
        'label' => t('Issuetype name'),
      ),
      'summary' => array(
        'type' => 'text',
        'label' => t('Summary'),
      ),
      'description' => array(
        'type' => 'text',
        'label' => t('Description'),
      ),
      'custom_jira_field_1_name' => array(
        'type' => 'text',
        'label' => t('Custom Jira field 1 name'),
        'optional' => true,
        'description' => t('The name of a custom field in Jira.  Ex: customfield_10130'),
      ),
      'custom_jira_field_1_value' => array(
        'type' => 'text',
        'label' => t('Custom Jira field 1 value'),
        'optional' => true,
        'description' => t('The value to be used for custom field 1.'),
      ),
      'custom_jira_field_2_name' => array(
        'type' => 'text',
        'label' => t('Custom Jira field 2 name'),
        'optional' => true,
        'description' => t('The name of a custom field in Jira.  Ex: customfield_10130'),
      ),
      'custom_jira_field_2_value' => array(
        'type' => 'text',
        'label' => t('Custom Jira field 2 value'),
        'optional' => true,
        'description' => t('The value to be used for custom field 2.'),
      ),
      'custom_jira_field_3_name' => array(
        'type' => 'text',
        'label' => t('Custom Jira field 3 name'),
        'optional' => true,
        'description' => t('The name of a custom field in Jira.  Ex: customfield_10130'),
      ),
      'custom_jira_field_3_value' => array(
        'type' => 'text',
        'label' => t('Custom Jira field 3 value'),
        'optional' => true,
        'description' => t('The value to be used for custom field 3.'),
      ),
    ),
    'provides' => array(
      'jira_instanceurl' => array(
        'type' => 'text',
        'label' => t('Jira instance URL'),
      ),
      'jira_issue_id' => array(
        'type' => 'integer',
        'label' => t('Jira issue ID'),
      ),
      'jira_issue_key' => array(
        'type' => 'text',
        'label' => t('Jira issue key'),
      ),
      'jira_issue_self' => array(
        'type' => 'text',
        'label' => t('Jira issue REST API endpoint'),
      ),
    ),
  );

  return $items;
}

/**
 * Implements hook_rules_condition_info().
 */
function jira_rest_rules_rules_condition_info() {
  $conditions['jira_rest_rules_credentials_exists'] = array(
    'label' => t('Jira REST credentials exists'),
    'group' => t('Jira REST Rules'),
  );

  return $conditions;
}
